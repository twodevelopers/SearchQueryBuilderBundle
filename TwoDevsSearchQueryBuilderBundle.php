<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 13.04.2015
 * Time: 14:01
 */

namespace TwoDevs\SearchQueryBuilderBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\EventDispatcher\DependencyInjection\RegisterListenersPass;

class TwoDevsSearchQueryBuilderBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new RegisterListenersPass(
            'search.dispatcher',
            'search.listener',
            'search.subscriber'
        ));

        $container->setDefinition('search.dispatcher', new Definition(
            'Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher',
            array(new Reference('service_container'))
        ));
    }
}
