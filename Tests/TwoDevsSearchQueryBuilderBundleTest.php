<?php
namespace TwoDevs\SearchQueryBuilderBundle;

/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 13.04.2015
 * Time: 14:14
 */

class TwoDevsSearchQueryBuilderBundleTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     */
    public function testBuild()
    {
        $container = $this->getMockBuilder('\Symfony\Component\DependencyInjection\ContainerBuilder')
            ->setMethods(array('addCompilerPass', 'setDefinition'))
            ->getMock();

        $container->expects($this->exactly(1))
            ->method('addCompilerPass')
            ->with($this->isInstanceOf('\Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface'));

        $bundle = new TwoDevsSearchQueryBuilderBundle();
        $bundle->build($container);
    }
}
